angular
    .module('input.validate', [
        'ui.validate',
        'ui.bootstrap'
    ])
    .directive('inputValidate', function($compile) {
        return {
            restrict: 'E',
            replace: true,
            require: '^?form',
            template: '',
            scope: {
                ngModel: '=',
                name: '@',
                type: '@',
                label: '@',
                validations: '<',
                options: '<',
                placeholder: '@'
            },
            link: function(scope, element, attrs, parentCtrl) {
                var template = '';
                var validators = '';
                scope.form = parentCtrl;
                scope.errorText = scope.form.$name + '.' + scope.name + '.';
                scope.customValidators = {};
                scope.customText = '{';

                var compileError = function() {
                    var custom = false;
                    try {
                        scope.validations.forEach(function(el) {
                            if (el.name == null) {
                                // Do nothing
                            } else {
                                if (angular.isUndefined(el.message)) {
                                    el.message = el.name;
                                }
                                if (angular.isUndefined(el.value) && angular.isUndefined(el.function)) {
                                    validators += el.name + ' ';
                                } else if (!angular.isUndefined(el.value) && angular.isUndefined(el.function)) {
                                    validators += el.name + '="' + el.value + '" ';
                                } else if (angular.isUndefined(el.value) && !angular.isUndefined(el.function)) {
                                    if (!custom) {
                                        validators += 'ui-validate="{{customText}}"';
                                        custom = true;
                                    } else scope.customText += ' , '
                                    scope.customValidators[el.name] = el.function;
                                    scope.customText += el.name + ':\'customValidators.' + el.name + '($value)\''
                                }
                            }
                        })
                    } catch (e) {
                        console.log(scope.errorText + '$error : No Validations Defined');
                    }

                    if (custom) scope.customText += '}';
                }

                scope.showIcon = function(){
                    if(scope.form[scope.name].$invalid && (scope.form[scope.name].$dirty || scope.form[scope.name].$touched)){
                        scope.iconShown = true;
                        return true;
                    }
                    else {
                        scope.iconShown = false;
                        return false;
                    }
                }

                scope.getTooltip = function() {
                    var error = scope.form[scope.name].$error;
                    var msg = '';
                    var ername = '';
                    try {
                        scope.validations.some(function(el) {
                            if (el.name == 'ng-pattern') {
                                ername = 'pattern';
                            } else {
                                ername = el.name;
                            }
                            if (error[ername]) {
                                msg = el.message;
                                return true;
                            }
                        })
                    } catch (e) {
                        // console.log(scope.errorText +'No validations given');
                    }
                    return msg;
                }

                var setTemplate = function() {
                        var temp = {};
                        compileError();
                        // Set default options
                        if (!scope.options) {
                            scope.options = {
                                'tooltipPlacement': 'bottom-right',
                                'iconClasses': 'glyphicon glyphicon-warning-sign',
                                'inputClasses': '',
                                'labelClasses': ''
                            }
                        } else {
                            if (!scope.options.iconClasses) {
                                scope.options.iconClasses = 'glyphicon glyphicon-warning-sign';
                            }
                            if (!scope.options.tooltipPlacement) {
                                scope.options.tooltipPlacement = 'bottom-right';
                            }
                        }

                        //Assemble template
                        temp.start = '<div class="form-group" ng-class="{\'has-error has-feedback\':iconShown}">';
                        temp.label = '<label class="control-label {{options.optClasses}}">{{label}}</label>';
                        temp.input = '<input class="form-control {{options.inputClasses}}" type="{{type}}" ng-model="ngModel" name="{{name}}" ' + validators + ' placeholder="{{placeholder}}">';
                        temp.icon = '<span uib-tooltip="{{getTooltip()}}" tooltip-placement="'+scope.options.tooltipPlacement+'" class="' + scope.options.iconClasses + ' form-control-feedback feedback-icon" ng-show="showIcon()"></span>';
                        temp.end = '</div>';
                        template = temp.start + ((scope.label) ? temp.label : '') + temp.input + temp.icon + temp.end;
                        // Set template
                        element.html(template);
                        $compile(element.contents())(scope);
                    }
                    // console.log(attrs);
                if (parentCtrl == null) {
                    // If no form
                    element[0].innerHTML = '<div>Must be inside form element</div>'
                } else if (parentCtrl.$name == '') {
                    // If no form name
                    element[0].innerHTML = '<div>Form element must have a name</div>'
                } else if (angular.isUndefined(attrs.ngModel)) {
                    // If no ngModel
                    element[0].innerHTML = '<div>No ng-model defined</div>'
                } else {
                    setTemplate();
                };
            }
        }
    })
