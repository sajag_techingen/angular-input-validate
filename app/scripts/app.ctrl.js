angular
    .module('validateInputApp')
    .controller('mainCtrl', function($scope) {
        // $scope.model=123;

        var customValidation = function(value) {
            if (value == 222) {
                return true;
            }
            return false
        }

        $scope.validations = [{
            'name': 'required',
            'message': 'This field is Required!'
        }, {
            'name': 'min',
            'value': 20,
            'message': 'Minimum value is 20'
        }, {
            'name': 'custom',
            'function': customValidation,
            'message': 'Must be equal to 222'
        }]

        $scope.validation2 = [{
            'name': 'maxLength',
            'value': 20,
            'message': 'Maximum length is 20'
        },{
            'name': 'ng-pattern',
            'value': '/^[0-9]/',
            'message': 'Pattern'
        }]


        $scope.checkError = function() {
            console.log('Potatoes');
        }
    })
