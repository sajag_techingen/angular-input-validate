# input-validate v1.05

## Requires
* [Bootstrap](http://getbootstrap.com/)
* [Angular ui bootstrap](https://github.com/angular-ui/bootstrap)
* [Angular ui Validate](https://github.com/angular-ui/ui-validate)

## Installation

* Include 
```html
    <script src=".../input-validate.js"></script>
```
* Add
```js
angular.module('myApp', [
	'ui.bootstrap',
	...
        'input.validate' <-- Add
]);
```

## Usage
* Must be included within `<form name="..."></form>`
* Use as follows
```html
<input-validate ng-model="Test" name="Name" type="text" label="Name" validations="validations" options="options">
```

### Attributes:
* ng-model : `Required` ngModel for input field
* name : `Required` Name of input field
* label : `Optional` Label for input field
* type: `Optional` 'text' by default
* placeholder: `Optional` Placeholder text
* validations: `Optional` Requires validation array as input (Shown below)
* options: `Optional` Requires options array as input (Shown below)
                

### Validation: (Is technically optional, but why use this without validation?)
```js
$scope.validations = [
{	// Regular validation
    'name': 'required', 
    'message': 'This field is Required!'
}, { // Validation with value
    'name': 'min',
    'value': 20,
    'message': 'Minimum value is 20'
}, { // Custom validation using a function (ui.validate)
    'name': 'custom',
    'function': customValidation,
    'message': 'Must be equal to 222'
}]

function customValidation(value) {
        if (value == 222) {
            return true;
        }
        return false
    }
```

### Optional attributes: (Not Required to use the directive)
```js
scope.options = {
    'tooltipPlacement': 'bottom-right', // Placement of tooltip
    'iconClasses': 'glyphicon glyphicon-warning-sign', // Icon classes
    'inputClasses': '', // Input field classes
    'labelClasses': '' // Label field classes
}
```